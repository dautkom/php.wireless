<?php

namespace dautkom\wireless\library;
use dautkom\wireless\Wireless;
use dautkom\wireless\iConnect;

/**
 * @package dautkom\wireless\library
 */
class SSH extends Wireless implements iConnect
{

    /**
     * Timeout in seconds
     * @var int
     */
    protected $timeout = 3;

    /**
     * Connection resource handler
     */
    protected $connection;

    /**
     * Stream resource
     */
    protected $stream;

    /**
     * @var array
     */
    protected $default_methods = [
        'kex' => 'diffie-hellman-group1-sha1, diffie-hellman-group14-sha1, and diffie-hellman-group-exchange-sha1'
    ];

    /**
     * Connect to device
     *
     * @param string $ip
     * @param int    $port
     * @param array  $methods
     *
     * @return resource
     * @throws \Exception
     */
    public function connect($ip, $port = 22, $methods = [])
    {

        ini_set('default_socket_timeout', $this->timeout);
        $auth_methods     = (is_array($methods) && !empty($methods)) ? $methods : $this->default_methods;
        $this->connection = @ssh2_connect($ip, $port, $auth_methods);
        ini_restore('default_socket_timeout');

        if (!$this->connection) {
            throw new \Exception('SSH Connect failed to ' . $ip);
        }

        return $this->connection;

    }


    /**
     * Authentication wrapper
     *
     * @param   string $login
     * @param   string $pass
     *
     * @throws \Exception
     * @return  bool
     */
    public function login(string $login, string $pass): bool
    {

        $auth = @ssh2_auth_password( $this->connection, $login, $pass );

        if (!$auth) {
            throw new \Exception('SSH Authentication failed');
        }

        return true;

    }


    /**
     * Send command
     *
     * @param   string $cmd
     *
     * @throws \Exception
     * @return  resource
     */
    public function send($cmd)
    {

        $this->stream = ssh2_exec( $this->connection, $cmd );

        if (!$this->stream){
            throw new \Exception("SSH Command execution failed");
        }

        return $this->stream;

    }


    /**
     * Return data which was read from stream
     *
     * @return string
     */
    public function getData(): string
    {

        stream_set_blocking( $this->stream, true );
        $data = '';

        while ( $buf = fread( $this->stream, 4096 ) ) {
            $data .= $buf;
        }

        fclose( $this->stream );
        return $data;

    }

}
