<?php

namespace dautkom\wireless\library;
use dautkom\wireless\Wireless;


/**
 * @package dautkom\wireless\library
 * @property Port $Port
 * @property Fdb  $Fdb
 */
abstract class Common extends Wireless
{

    /**
     * Object for interacting with Port info
     * @var  Port
     */
    public $Port;

    /**
     * Object for interacting with FDB table
     * @var  Fdb
     */
    public $Fdb;

    /**
     * @var array
     */
    protected static $common_filter = array(
        "/Hex:/",
        "/Hex-/",
        "/x:/",
        "/x-/",
        "/ /",
        "/\./",
        "/Hex-STRING:/"
    );

    /**
     * Load ini file
     * @ignore
     */
    public function __construct()
    {
        $this->loadIni();
    }


    /**
     * Retrieve vendor name
     *
     * @return string
     */
    public function getVendor()
    {
        return ucfirst(substr(self::$model_id, 0, strpos(self::$model_id, '\\')));
    }


    /**
     * Retrieve device name from sysName.0
     *
     * @return string
     */
    public function getName()
    {
        return $this->get('.1.3.6.1.2.1.1.5.0');
    }


    /**
     * Retrieve uptime.
     *
     * Return value consists of timeticks by default. 1 tTick = 1/10 sec.
     * Raw output can be retrieved if method is called as getUptime(true).
     *
     * If no timeticks where found in result string, the raw outupt will
     * be returned regardless of argument value
     *
     * @param  bool   $raw if true the raw string will be returned
     * @return string
     */
    public function getUptime($raw = false)
    {

        $uptime = $this->get('.1.3.6.1.2.1.1.3.0');
        $tticks = array();

        preg_match( '/\((\d+)\)/', $uptime, $tticks );

        if( !$raw && array_key_exists(1, $tticks) && $tticks[1] > 0 ) {
            return $tticks[1];
        }

        return $uptime;

    }


    /**
     * Retrieve device location from sysLocation.0
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->get('.1.3.6.1.2.1.1.6.0');
    }


    /**
     * Retrieve device contact information from sysContact.0
     *
     * @return string
     */
    public function getContact()
    {
        return $this->get('.1.3.6.1.2.1.1.4.0');
    }


    /**
     * Get access point SSID
     *
     * @return string
     */
    public function getSsid()
    {
        return $this->get(self::$device['snmp']['mtxrWlStatSsid']);
    }


    /**
     * Get access point MAC adress
     *
     * @return mixed
     */
    public function getApMac()
    {
        $snmp = preg_replace( self::$common_filter, '', $this->get(self::$device['snmp']['mtxrWlStatBssid']) );
        return $snmp;
    }


    /**
     * Get access point frequency
     *
     * @return string
     */
    public function getApFrequency()
    {
        return $this->get(self::$device['snmp']['mtxrWlStatFreq']);
    }


    /**
     * Get access point signals
     *
     * @return array
     */
    public function getApSignals()
    {
        return array(
            'strength' => $this->get(self::$device['snmp']['mtxrWlRtabStrength']),
            'tx_rate'  => $this->get(self::$device['snmp']['mtxrWlRtabTxRate']),
            'rx_rate'  => $this->get(self::$device['snmp']['mtxrWlRtabRxRate']),
        );
    }


    /**
     * Get data from Access point
     *
     * @param  string $cmd
     * @return string
     * @throws \Exception
     */
    protected function getDataFromAP($cmd)
    {
        $ssh = new SSH();

        // Connect and login to device
        $ssh->connect(self::$ip);
        $ssh->login(self::$ssh[0], self::$ssh[1]);

        // Send command
        $ssh->send($cmd);

        // Get response
        $data = $ssh->getData();

        return $data;
    }


    /**
     * Retrieve device name
     *
     * @return string
     */
    abstract public function getDeviceName();


    /**
     * Retrieve product name
     *
     * @return string
     */
    abstract public function getProductName();


    /**
     * Retrieve product version
     *
     * @return string
     */
    abstract public function getProductVersion();


    /**
     * Get data from access point
     *
     * @return array
     */
    abstract public function getApInfo();

}
