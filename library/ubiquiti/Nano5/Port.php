<?php

namespace dautkom\wireless\library\ubiquiti\Nano5;
use dautkom\wireless\library\ubiquiti\PortTrait;


/**
 * @package dautkom\wireless\library\ubiquiti\Nano5
 */
class Port extends \dautkom\wireless\library\Port
{

    /**
     * Trait for getOperatingSpeed and getLink methods
     *
     * @see PortTrait::getOperatingSpeed()
     * @see PortTrait::getLink()
     */
    use PortTrait;

}
