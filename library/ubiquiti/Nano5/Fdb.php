<?php

namespace dautkom\wireless\library\ubiquiti\Nano5;
use dautkom\wireless\library\ubiquiti\FdbTrait;


/**
 * @package dautkom\wireless\library\ubiquiti\Nano5
 */
class Fdb extends \dautkom\wireless\library\ubiquiti\Fdb
{

    /**
     * Trait for getFdbTable method
     *
     * @see FdbTrait::getFdbTable()
     */
    use FdbTrait;

}
