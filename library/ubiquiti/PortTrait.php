<?php

namespace dautkom\wireless\library\ubiquiti;


/**
 * Trait for library\ubiquiti\Port, intended to be used for the sake of DRY
 * @package dautkom\wireless\library\ubiquiti
 */
trait PortTrait
{

    /**
     * An estimate of the interface's current bandwidth in bits per second.
     * ifSpeed don't work on AirOS 4.x
     *
     * @return array
     */
    public function getOperatingSpeed(): array
    {
        return ['eth0' => null];
    }


    /**
     * The current operational state of the interface.
     * ifSpeed don't work on AirOS 4.x
     * To check link state use @see Port::getOperatingStatus()
     *
     * @return array
     */
    public function getLink(): array
    {
        return ['eth0' => null];
    }

}
