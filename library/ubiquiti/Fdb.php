<?php

namespace dautkom\wireless\library\ubiquiti;


/**
 * @package dautkom\wireless\library\ubiquiti
 */
class Fdb extends \dautkom\wireless\library\Fdb
{

    /**
     * Get FDB table
     *
     * @return array
     */
    public function getFdbTable()
    {

        // Get data from Access point
        $data = json_decode(parent::getDataFromAP('/usr/bin/brmacs'), true);

        if (!is_array($data)) {
            trigger_error( 'Error while retrieving FDB table', E_USER_WARNING );
            return null;
        }

        // Get only eth ports
        $fdb = array_filter($data['brmacs'], function($ar) {
            return (preg_match('/eth/', $ar['port']));
        });

        // Reverse ports
        krsort($fdb);

        // Return data and reset array keys
        return array_values($fdb);

    }

}
