<?php

namespace dautkom\wireless\library\ubiquiti;


/**
 * Trait for library\ubiquiti\Fdb, intended to be used for the sake of DRY
 * @package dautkom\wireless\library\ubiquiti
 */
trait FdbTrait
{

    /**
     * Get FDB table
     *
     * @return array
     */
    public function getFdbTable(): array
    {

        /** @noinspection PhpUndefinedClassInspection, PhpUndefinedMethodInspection
         *  Get data from Access point via SSH
         */
        $data   = parent::getDataFromAP('brctl showmacs br0');
        $ports  = array_flip(self::$device['brctl']);
        $result = [];

        // Parse data and push it to result array
        if (preg_match_all('/(\d+)\s+([0-9a-f\-:]+)\s+(\w+)\s+(\d*(?:\.\d+)?)/', $data, $matches)) {

            // Rearrange array
            $count = count( $matches[0] );

            for ( $i = 0; $i < $count; $i++ ) {
                if ($matches[1][$i] == '1' && $matches[3][$i] == 'no') {
                    $result[] = [
                        'bridge' => 'br0',
                        'port'   => $ports[$matches[1][$i]],
                        'hwaddr' => $matches[2][$i],
                        'ageing' => $matches[4][$i],
                    ];
                }
            }

        }

        return $result;

    }

}
