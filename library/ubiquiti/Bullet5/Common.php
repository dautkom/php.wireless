<?php

namespace dautkom\wireless\library\ubiquiti\Bullet5;


/**
 * @package dautkom\wireless\library\ubiquiti\Bullet5
 */
class Common extends \dautkom\wireless\library\ubiquiti\Common
{

    /**
     * @ignore
     */
    public function __construct()
    {
        parent::__construct();
        $this->Port = new Port();
        $this->Fdb  = new Fdb();
    }

}
