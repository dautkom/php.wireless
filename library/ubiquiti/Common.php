<?php

namespace dautkom\wireless\library\ubiquiti;


/**
 * @package dautkom\wireless\library\ubiquiti
 */
class Common extends \dautkom\wireless\library\Common
{

    /**
     * Retrieve device name
     *
     * @return string
     */
    public function getDeviceName()
    {
        return $this->get( self::$device['snmp']['dot11manufacturerName'] );
    }


    /**
     * Retrieve product name
     *
     * @return string
     */
    public function getProductName()
    {
        return $this->get( self::$device['snmp']['dot11manufacturerProductName'] );
    }


    /**
     * Retrieve product version
     *
     * @return string
     */
    public function getProductVersion()
    {
        return $this->get( self::$device['snmp']['dot11manufacturerProductVersion'] );
    }


    /**
     * Get data from access point
     *
     * @return array
     */

    public function getApInfo()
    {

        // Get config from AP
        $data = parent::getDataFromAP('cat /tmp/system.cfg');

        if (empty($data)) {
            trigger_error( 'Error while retrieving config', E_USER_WARNING );
            return null;
        }

        if (!preg_match_all('/(radio.1.mode=)(\w+)/', $data, $w_mode)) {
            trigger_error( 'Error while retrieving wireless mode', E_USER_WARNING );
            return null;
        }

        // Security mode convert
        $sec_convert = [
            'TKIP CCMP' => 'WPA2',
            'TKIP'      => 'WPA2-TKIP',
            'CCMP'      => 'WPA2-AES',
        ];

        // Wireless mode convert
        $mode_convert = [
            'managed' => 'Station',
            'master'  => 'AP',
        ];

        switch($w_mode[2][0]) {

            case 'managed':
                // Parse config
                preg_match_all('/(wds.status=|1.wds=)(\w+)/', $data, $wds_status);
                preg_match_all('/(pairwise.1.name=)(\w+)/', $data, $security);
                preg_match_all('/(network.1.psk=)(\w+)/', $data, $wpa_key);
            break;

            case 'master':
                // Parse config
                preg_match_all('/(wireless.1.wds=)(\w+)/', $data, $wds_status);
                preg_match_all('/(wpa.1.pairwise=)(\w+)/', $data, $security);
                preg_match_all('/(wpa.psk=)(\w+)/', $data, $wpa_key);
            break;

        }

        // Push data to array
        $result = [
            'w_mode'     => (array_key_exists($w_mode[2][0], $mode_convert)) ? $mode_convert[$w_mode[2][0]] : '',
            'wds_status' => (!empty($wds_status[2])) ? $wds_status[2][0] : '',
            'sec_type'   => (!empty($security[2]) && array_key_exists($security[2][0], $sec_convert)) ? $sec_convert[$security[2][0]] : '',
            'wpa_key'    => (!empty($wpa_key[2])) ? $wpa_key[2][0] : '',
        ];

        return $result;

    }

}
