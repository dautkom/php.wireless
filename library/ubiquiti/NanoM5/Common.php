<?php

namespace dautkom\wireless\library\ubiquiti\NanoM5;
use dautkom\wireless\library\{
    Port, ubiquiti\Fdb
};


/**
 * NanoStation M5 Common class overrides
 *
 * @package Wireless\library\ubiquiti\NanoM5
 */
class Common extends \dautkom\wireless\library\ubiquiti\Common
{

    /**
     * @ignore
     */
    public function __construct()
    {
        parent::__construct();
        $this->Port = new Port();
        $this->Fdb  = new Fdb();
    }

}
