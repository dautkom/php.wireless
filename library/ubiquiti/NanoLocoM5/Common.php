<?php

namespace dautkom\wireless\library\ubiquiti\NanoLocoM5;
use dautkom\wireless\library\{
    Port, ubiquiti\Fdb
};


/**
 * @package dautkom\wireless\library\ubiquiti\NanoLocoM5
 */
class Common extends \dautkom\wireless\library\ubiquiti\Common
{

    /**
     * @ignore
     */
    public function __construct()
    {
        parent::__construct();
        $this->Port = new Port();
        $this->Fdb  = new Fdb();
    }

}
