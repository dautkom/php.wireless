<?php

namespace dautkom\wireless\library;
use dautkom\wireless\Wireless;


/**
 * @package dautkom\wireless\library
 */
abstract class Fdb extends Wireless
{

    /**
     * Get FDB table
     *
     * @return array
     */
    abstract public function getFdbTable();


    /**
     * Get data from Access point
     *
     * @param  string $cmd
     * @return string
     * @throws \Exception
     */
    protected function getDataFromAP($cmd)
    {
        $ssh = new SSH();

        // Connect and login to device
        $ssh->connect(self::$ip);
        $ssh->login(self::$ssh[0], self::$ssh[1]);

        // Send command
        $ssh->send($cmd);

        // Get response
        $data = $ssh->getData();

        return $data;
    }

}
