<?php

namespace dautkom\wireless\library;
use dautkom\wireless\Wireless;


/**
 * @package dautkom\wireless\library
 */
class Port extends Wireless
{

    /**
     * Retrieve port amount
     *
     * @return int
     */
    public function count()
    {
        return count(self::$device['ports']);
    }


    /**
     * Retrieves device ports
     *
     * @return array
     */
    public function getDevicePorts()
    {
        return array_keys(self::$device['ports']);
    }


    /**
     * Get port admin state
     *
     * Return array (
     *      [eth0] => value
     *      ...
     * )
     *
     * Value list:
     *  1 : enabled
     *  2 : disabled
     *
     * @return array
     */
    public function getPortState()
    {

        $ports  = self::$device['ports'];
        $result = [];

        foreach ($ports as $port => $value) {
            $result[$port] = preg_replace('/[^\d]/', '', $this->get(".1.3.6.1.2.1.2.2.1.7.$value"));
        }

        return array_map(function($val) { return intval(!boolval(intval($val) - 1)); }, $result);

    }


    /**
     * The current operational state of the interface.
     *
     * Return array (
     *      [eth0] => value
     *      ...
     * )
     *
     * Value list:
     *  1: up
     *  2: down
     *  3: testing
     *  4: unknown
     *  5: dormant
     *  6: notPresent
     *  7: lowerLayerDown
     *
     * @return array
     */
    public function getOperatingStatus()
    {

        $ports  = self::$device['ports'];
        $result = [];

        foreach ($ports as $port => $value) {
            $result[$port] = preg_replace('/[^\d]/', '', $this->get(".1.3.6.1.2.1.2.2.1.8.$value"));
        }

        return $result;

    }

    /**
     * An estimate of the interface's current bandwidth in bits per second.
     *
     * @return array
     */
    public function getOperatingSpeed()
    {

        $ports  = self::$device['ports'];
        $result = [];

        foreach ($ports as $port => $value) {
            $result[$port] = $this->get(".1.3.6.1.2.1.2.2.1.5.$value");
        }

        return $result;

    }


    /**
     * The current operational state of the interface.
     *
     * Return array (
     *      [eth0] => value
     *      ...
     * )
     *
     * Value list:
     *  0: link down
     *  1: 10Mbps
     *  2: 100Mbps
     *  3: 1Gbps
     *
     * @return array|int|null
     */
    public function getLink()
    {

        $opStatus = $this->getOperatingStatus();
        $opSpeed  = $this->getOperatingSpeed();

        if (!is_array($opStatus) || !is_array($opSpeed)) {
            return null;
        }

        $link = [];

        foreach ($opSpeed as $port => $speed) {
            if ($opStatus[$port] == 1 && $speed > 0) {
                $link[$port]  = $speed;              // Set speed to each port
                $link[$port] /= 1000000;             // Get operating speed in Mbit/s
                $link[$port]  = log10($link[$port]); // Prepare return value

            }
            else {
                $link[$port] = 0;
            }
        }

        return ($link < 0) ? 0 : $link;

    }

    /**
     * The type of interface.
     *
     * @return array
     */
    public function getIfType()
    {

        $ports  = self::$device['ports'];
        $result = [];

        foreach ($ports as $port => $value) {
            $result[$port] = $this->get(".1.3.6.1.2.1.2.2.1.3.$value");
        }

        return $result;

    }


    /**
     * Retrieves amount of sent/recieved packets on port.
     *
     * @return array|bool
     */
    public function getDataFlow()
    {

        $ports  = self::$device['ports'];
        $result = [];

        foreach ($ports as $port => $value) {

            $result[$port] =  array(
                'in' => array(
                    'unicast'    => intval($this->get(".1.3.6.1.2.1.2.2.1.11.$value")),
                    'nonunicast' => intval($this->get(".1.3.6.1.2.1.2.2.1.12.$value"))
                ),

                'out' => array(
                    'unicast'    => intval($this->get(".1.3.6.1.2.1.2.2.1.17.$value")),
                    'nonunicast' => intval($this->get(".1.3.6.1.2.1.2.2.1.18.$value")),
                ),
            );

        }

        return $result;

    }


    /**
     * Retrieves amount of errors/discards on port.
     *
     * @return array|bool
     */
    public function getErrors()
    {

        $ports  = self::$device['ports'];
        $result = [];

        foreach ($ports as $port => $value) {

            $result[$port] =  array(
                'in' => array(
                    'errors'   => intval($this->get(".1.3.6.1.2.1.2.2.1.14.$value")),
                    'discards' => intval($this->get(".1.3.6.1.2.1.2.2.1.13.$value")),
                ),
                'out' => array(
                    'errors'   => intval($this->get(".1.3.6.1.2.1.2.2.1.20.$value")),
                    'discards' => intval($this->get(".1.3.6.1.2.1.2.2.1.19.$value")),
                ),
            );

        }

        return $result;

    }

}
