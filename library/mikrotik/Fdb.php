<?php

namespace dautkom\wireless\library\mikrotik;

/**
 * @package dautkom\wireless\library\mikrotik
 */
class Fdb extends \dautkom\wireless\library\Fdb
{

    /**
     * Get FDB table
     *
     * @return array
     */
    public function getFdbTable(): array
    {

        // Get data from Access point
        $data   = parent::getDataFromAP('interface bridge host print where on-interface=ether1');
        $ports  = array_flip(self::$device['interface']);
        $result = [];

        // Parse data and push it to result array
        if (preg_match_all('/(E|\s+)\s+((?:\-\w|\w)+)\s+((?:[0-9a-f]{2}[:-]){5}[0-9a-f]{2})\s+((?:\-\w|\w)+)\s+(\w+)/i', $data, $matches)) {

            // Rearrange array
            $count = count( $matches[0] );

            for ( $i = 0; $i < $count; $i++ ) {
                $result[] = [
                    'bridge' => $matches[2][$i],
                    'port'   => $ports[$matches[4][$i]],
                    'hwaddr' => $matches[3][$i],
                    'ageing' => $matches[5][$i],
                ];
            }

        }

        return $result;

    }

}
