<?php

namespace dautkom\wireless\library\mikrotik;
use dautkom\wireless\library\{
    SSH, Port
};


/**
 * @package dautkom\wireless\library\mikrotik
 */
class Common extends \dautkom\wireless\library\Common
{

    /**
     * @ignore
     */
    public function __construct()
    {
        parent::__construct();
        $this->Port = new Port();
        $this->Fdb  = new Fdb();
    }


    /**
     * Retrieve device name
     *
     * @return string
     */
    public function getDeviceName()
    {
        return $this->getBoardData('board-name');
    }


    /**
     * Retrieve product name
     *
     * @return string
     */
    public function getProductName()
    {
        return $this->getBoardData('platform');
    }


    /**
     * Retrieve product version
     *
     * @return string
     */
    public function getProductVersion()
    {
        return $this->get('.1.3.6.1.4.1.14988.1.1.4.4.0');
    }


    /**
     * Get data from access point
     *
     * @return array
     */
    public function getApInfo()
    {

        // Get config from AP
        $data = parent::getDataFromAP('interface wireless print where name=wlan1');

        if (empty($data)) {
            trigger_error( 'Error while retrieving config', E_USER_WARNING );
            return null;
        }

        // Parse config
        preg_match_all('/([\s]mode=)(\w+\-\w+|\w+)/', $data, $w_mode);
        preg_match_all('/(wds-mode=)(\w+)/', $data, $wds_status);

        // Get data from security-profile
        if (preg_match_all('/(security-profile=)(\w+)/', $data, $sec_prof)) {

            $profile_data = parent::getDataFromAP('interface wireless security-profiles print where name=' . $sec_prof[2][0] );
            preg_match_all('/(unicast-ciphers=)(\w+\-\w+|\w+)/', $profile_data, $security);
            preg_match_all('/(wpa2-pre-shared-key=")(\w+)/', $profile_data, $wpa_key);

        }
        
        // Push data to array
        $result = [
            'w_mode'     => (!empty($w_mode[2]))     ? $w_mode[2][0]     : '',
            'wds_status' => (!empty($wds_status[2])) ? $wds_status[2][0] : '',
            'sec_type'   => (!empty($security[2]))   ? $security[2][0]   : '',
            'wpa_key'    => (!empty($wpa_key[2]))    ? $wpa_key[2][0]    : '',
        ];

        return $result;

    }


    /**
     * Get data from board
     *
     * @param  string $search
     * @throws \Exception
     * @return string
     */
    protected function getBoardData($search)
    {

        $ssh = new SSH();

        // Connect and login to device
        $ssh->connect(self::$ip);
        $ssh->login(self::$ssh[0], self::$ssh[1]);

        // Send command
        $ssh->send('system resource print');

        // Get response
        $data = $ssh->getData();

        $resource = '';

        if ( preg_match_all('/(' . $search . ':)\s+(\S+)/', $data, $matches ) ) {
            $resource = implode($matches[2]);
        }

        return $resource;

    }

}
