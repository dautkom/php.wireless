<?php

namespace dautkom\wireless;
use dautkom\netsnmp;


/**
 * Interface iConnect for warning mitigation:
 * Strict Standards: Declaration of SSH::connect() should be compatible with that of Wireless::connect()
 *
 * @package dautkom\netswitch
 */
interface iConnect
{
    /**
     * @param  string $arg1 IP-address
     * @param  mixed  $arg2 SSH port or SNMP settings array
     * @param  mixed  $arg3 SSH settings array
     * @return object
     */
    public function connect($arg1, $arg2, $arg3);
}


/**
 * @package dautkom\wireless
 */
class Wireless implements iConnect
{

    /**
     * Device IP-address
     * @var string
     */
    protected static $ip;

    /**
     * SSH credentials as array[login, password]
     * @var array
     */
    protected static $ssh = ['admin', 'password'];

    /**
     * SNMP instance
     * @var netsnmp\NetSNMP
     */
    protected static $snmp;

    /**
     * Vendor name and sysObjectID match
     * @see Device::connect()
     * @var array
     */
    protected static $vendors = [
        // ubiquiti
        10002 => [
            '1' => [
                'ubiquiti\\Bullet5'    => ['product' => 'Bullet5'],
                'ubiquiti\\Nano5'      => ['product' => 'NanoStation5'],
            ],
        ],
        41112 => [
            '1.4' => [
                'ubiquiti\\NanoLocoM5'   => ['product' => 'NanoStation Loco M5'],
                'ubiquiti\\NanoM5'       => ['product' => 'NanoStation M5'],
                'ubiquiti\\NanoBridgeM5' => ['product' => 'NanoBridge M5'],
            ],
        ],
        // Mikrotik
        14988 => [
            '1' => 'mikrotik',
        ],
    ];

    /**
     * SNMP attributes
     * Add any attribute and use any combination (or all scope) to determine device model namespace by given OID
     *
     * @see Device::connect()
     * @var array
     */
    protected static $snmpAttributes = [
        // ubiquiti
        10002 => [
            'product' => '.1.2.840.10036.3.1.2.1.3',
        ],
        // ubiquiti
        41112 => [
            'product' => '.1.2.840.10036.3.1.2.1.3',
        ],
    ];

    /**
     * Device ID, similar to namespace
     * @var string
     */
    protected static $model_id;

    /**
     * @var array
     */
    public static $device;


    /**
     * Factory method
     *
     * @throws \Exception
     * @param  string $ip
     * @param  array  $snmp
     * @param  array  $ssh
     * @return mixed
     */
    public function connect($ip, $snmp=[], $ssh=[])
    {

        self::$ip   = trim($ip);
        self::$ssh  = array_replace(self::$ssh, $ssh);

        if( !filter_var(self::$ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ) {
            throw new \RuntimeException( 'Wrong IP-address specified' );
        }

        // Instantiate SNMP engine and get sysObjectID
        self::$snmp = (new netsnmp\NetSNMP())->init($ip, $snmp, 0);

        try {
            $device = @self::$snmp->get(".1.3.6.1.2.1.1.2.0");
        }
        catch(\TypeError $e) {
            $device = false;
        }

        if( !$device ) {
            throw new \RuntimeException("Unable to connect to device $ip Check if device is offline or has misconfigured SNMP settings.");
        }

        // Split sysObjectID to vendor id and object
        $device = preg_split('/\./', $device, 2);

        // Check if device is supported by PHP.Wireless
        if( !array_key_exists($device[0], self::$vendors) ) {
            throw new \RuntimeException("Device vendor for sysObjectID.".implode('.', $device)." is not supported by PHP.Wireless");
        }
        elseif( !array_key_exists($device[1], self::$vendors[$device[0]]) ) {
            throw new \RuntimeException("Device with ID $device[1] is not supported by PHP.Wireless, while vendor is found for sysObjectID.$device[0]");
        }
        else {
            if( !is_array(self::$vendors[$device[0]][$device[1]]) ) {
                // Default behavior, when vendor object id points to namespace
                $device = self::$vendors[$device[0]][$device[1]];
            }
            else {
                // If there's an array in particular vendor configuration
                // try to find out required namespace by snmp attributes
                $device = $this->getNamespaceByAttributes($device[0], $device[1]);
            }
        }

        // Get full class name for device
        $class = "dautkom\\wireless\\library\\$device\\Common";
        self::$model_id = $device;

        return new $class;

    }


    /**
     * Wrapper for snmpget(). Fetch an SNMP object. Accepts both strings and arrays as OIDs.
     * When $oid is an array and keys in results will be taken exactly as in object_id.
     *
     * @param  array|string $oid SNMP object identifier
     * @return mixed
     */
    protected function get($oid)
    {

        // Error suppression is causing TypeError in prepareResult() method
        try {
            $snmp_get = self::$snmp->get($oid);
        }
        catch (\TypeError $e) {
            $snmp_get = false;
        }

        return $snmp_get;

    }


    /**
     * Wrapper for snmpwalk() and snmprealwalk(). Fetch SNMP object subtree
     *
     * @param  string $oid  SNMP object identifier representing root of subtree to be fetched
     * @param  bool   $real [optional] By default full OID notation is used for keys in output array. If set to <b>TRUE</b> subtree prefix will be removed from keys leaving only suffix of object_id.
     * @return array
     */
    protected function walk(string $oid, bool $real = false): array
    {

        // Error suppression is causing TypeError in prepareResult() method
        try {
            $snmp_walk = self::$snmp->walk($oid, $real);
        }
        catch (\TypeError $e) {
            $snmp_walk = [];
        }

        return $snmp_walk;

    }


    /**
     * Load ini file with device configuration
     *
     * @throws \RuntimeException
     * @return void
     * @throws \ReflectionException
     */
    protected function loadIni()
    {

        // Who called me?
        $invoker = new \ReflectionClass($this);
        $invoker = $invoker->getNamespaceName();

        // Create path to ini file
        $ini = __DIR__ . str_replace([ __NAMESPACE__, '\\'], ['', DIRECTORY_SEPARATOR], $invoker) . DIRECTORY_SEPARATOR . 'model.ini';

        if( !file_exists( $ini ) ) {
            throw new \RuntimeException( "$ini not found" );
        }

        self::$device = $this->parseIni($ini);

    }


    /**
     * Parse and validate ini file
     *
     * @param  string $ini path to ini file
     * @throws \RuntimeException
     * @return array
     */
    private function parseIni(string $ini): array
    {

        $ini = parse_ini_file( $ini, true );

        if( empty($ini) ) {
            throw new \RuntimeException('Error while loading '.self::$model_id.' ini-file, probably syntax error found');
        }

        if( !array_key_exists('ports', $ini) || empty($ini['ports']) ) {
            throw new \RuntimeException('No ports defined in '.self::$model_id.' ini-file');
        }

        return $ini;

    }


    /**
     * Namespace detection by snmp attributes
     *
     * @throws \RuntimeException
     * @param  string $vendor
     * @param  string $id
     * @return string
     */
    private function getNamespaceByAttributes( string $vendor, string $id ): string
    {

        // Obtain the necessary initialization parameters
        $options = array_keys( reset( self::$vendors[$vendor][$id] ) );
        $find    = array_flip( $options );  // values <--> keys

        array_walk( $find, function( &$value, $index, $vendor ) {

            if( !isset( self::$snmpAttributes[$vendor][$index] ) ) {
                throw new \RuntimeException("Unable to connect to device ".self::$ip.". Unknown attribute - $index.");
            }
            // Get attribute value
            $value = self::$snmp->walk( self::$snmpAttributes[$vendor][$index] );

            // Get firsts array element
            if( is_array($value) ) {
                $value = strtolower(reset($value));
            }

            if( !$value ) {
                throw new \RuntimeException("Unable to connect to device ".self::$ip.". Check if device is offline or has misconfigured SNMP settings.");
            }
        }
        , $vendor );

        /** Convert $vendors values to lowercase */
        array_walk_recursive(self::$vendors[$vendor][$id], function(&$value) {
            $value = strtolower($value);
        });

        $result = array_search( $find, self::$vendors[$vendor][$id] );

        // Check if device is supported by PHP.Wireless
        if( !$result ) {
            throw new \RuntimeException("Device sysObjectID.$vendor.$id is not supported by PHP.Wireless");
        }

        return $result;

    }

}
