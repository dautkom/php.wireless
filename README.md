# Wireless PHP library

PHP classes library for Ubiquity and MikroTik wireless devices management.

## Requirements

* PHP 7.0 or higher
* dautkom/netsnmp package
* php_snmp extension
* php_ssh2 extension
_Due to absence of SSH2 extension, library will not work in Windows_

## Supported models

* Ubiquity Bullet 5
* Ubiquity Nano 5
* Ubiquity Nano Bridge M5
* Ubiquity Nano Loco M5
* Ubiquity Nano M5
* Mikrotik Routerboard

## License

Copyright (c) 2013-2015 Imants Černovs and respective contributors under the [MIT License](http://opensource.org/licenses/MIT).
